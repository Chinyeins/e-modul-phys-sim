﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class grabThrowable : MonoBehaviour
{
    VRTK_ObjectAutoGrab autoGrab;

    private void Awake()
    {
        if (GetComponent<VRTK_ControllerEvents>() == null)
        {
            VRTK_Logger.Error(VRTK_Logger.GetCommonMessage(VRTK_Logger.CommonMessageKeys.REQUIRED_COMPONENT_MISSING_FROM_GAMEOBJECT, "VRTK_ControllerEvents_ListenerExample", "VRTK_ControllerEvents", "the same"));
            return;
        }

        GetComponent<VRTK_ControllerEvents>().TriggerPressed += new ControllerInteractionEventHandler(SnapobjectToController);
        GetComponent<VRTK_ControllerEvents>().TriggerReleased += new ControllerInteractionEventHandler(UnSnapObject);
        autoGrab = GetComponent<VRTK_ObjectAutoGrab>();
        autoGrab.enabled = false;
    }

    private void UnSnapObject(object sender, ControllerInteractionEventArgs e)
    {

        try
        {
            VRTK_InteractGrab grab = GetComponent<VRTK_InteractGrab>();
            GameObject grabbedThrowable = grab.GetGrabbedObject();
            
            //grab.ForceRelease();

        } catch(Exception error)
        {
            Debug.LogWarning(error.ToString());
        }
        
    }

    private void SnapobjectToController(object sender, ControllerInteractionEventArgs e)
    {
        GameObject throwable = null;
        GameObject grabbedObject = GameObject.Find("grabbedObject");
        VRTK_InteractGrab grab = GetComponent<VRTK_InteractGrab>();

        try
        {
            foreach (Transform child in grabbedObject.transform)
            {
                throwable = child.gameObject;
                throwable.transform.parent = null;
                throwable.transform.position = transform.position;
                throwable.GetComponent<Rigidbody>().isKinematic = false;
                grab.AttemptGrab();
                //autoGrab.objectToGrab = throwable.GetComponent<VRTK_InteractableObject>();
            }
        } catch (Exception error)
        {
            Debug.LogWarning("Can not snap object to right controller - " + error.ToString());
        }
    }

}
