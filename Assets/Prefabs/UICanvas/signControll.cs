﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class signControll : MonoBehaviour {
    public float upFresh = 0;

    private IEnumerator destroySign(int seconds, GameObject objectToDelete)
    {
        yield return new WaitForSeconds(seconds + upFresh);
        objectToDelete.GetComponentInParent<InstructionCanvasController>().HasSign = false;
        Destroy(objectToDelete);
    }

    public void destroyAfter(int sec, GameObject ob)
    {
        StartCoroutine(destroySign(sec, ob));
    }

}
