﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionCanvasController : MonoBehaviour {
    public GameObject InstructionCanvasPrefab;
    public GameObject Sign;
    public bool HasSign = false;
    private GameObject signController;
    private string message;
    public float SignFloorDistance = 0.5f;
    public int destroySignsAfterSeconds = 3;
    float timer = 0;
    GameObject cameraUsed;
    private bool IsVR = false;

    private void Awake()
    {
        GameObject nonVRCam = GameObject.Find("FirstPersonCharacter");
        GameObject vrCam = GameObject.Find("Camera");

        if(nonVRCam != null && nonVRCam.activeSelf)
        {
            cameraUsed = nonVRCam;
            IsVR = false;
        } else
        {
            cameraUsed = vrCam;
            IsVR = true;
        }
        
    }


    public signControll getSignController()
    {
        if(Sign != null)
        {
            return Sign.gameObject.GetComponent<signControll>();
        } else
        {
            return null;
        }
    }

 
    
    public void createSign(string message, float signFloorDistance, Quaternion rotation)
    {
        float dist = (signFloorDistance > 0f) ? signFloorDistance : SignFloorDistance;
        Vector3 position = new Vector3(transform.position.x, transform.position.y + dist, transform.position.z);

        if(GetComponentInParent<Rigidbody>().IsSleeping() || (!GetComponentInParent<Rigidbody>().isKinematic  && GetComponentInParent<Rigidbody>().IsSleeping() )) {
            
            try
            {
                Sign = Instantiate(InstructionCanvasPrefab, position, rotation, transform);
                HasSign = true;
            } catch (Exception e)
            {
                Debug.LogWarning("couldnt draw sign - " + e.ToString());
                HasSign = false;
            }
            
        }  

    }


    public void disableSign()
    {
        try
        {
            signControll _sign = getSignController();
            _sign.destroyAfter(0, Sign);
            HasSign = false;
        } catch (Exception e)
        {
            //Debug.Log(e.ToString());
        }
        
    }


    public void destroyAfter(int seconds)
    {
        try
        {
            signControll _sign = getSignController();
            _sign.destroyAfter(seconds, Sign);
            HasSign = false;
        } catch (Exception e)
        {
           //Debug.Log(e.ToString());
        }
        
    }


    private void FixedUpdate()
    {
        
        if(Sign != null && cameraUsed != null)
        {
            try
            {
                Sign.transform.rotation = InverseVRView(cameraUsed.transform, 180);
            } catch (Exception e)
            {
                Debug.LogWarning(e.ToString());
            }
            
        }
    }

    private Quaternion InverseVRView(Transform cam, int inverseDeg)
    {
        if(IsVR)
        {
            return new Quaternion(cam.rotation.x, cam.rotation.y - inverseDeg, cam.rotation.z, cam.rotation.w);
        } else
        {
            return new Quaternion(cam.rotation.x, cam.rotation.y, cam.rotation.z, cam.rotation.w);
        }
    }



}
