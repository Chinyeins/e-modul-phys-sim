﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class signDestroy : MonoBehaviour {

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("MainCamera"))
        {
            InstructionCanvasController icc = gameObject.GetComponentInParent<InstructionCanvasController>();
            icc.destroyAfter(5);
        }
    }
}
