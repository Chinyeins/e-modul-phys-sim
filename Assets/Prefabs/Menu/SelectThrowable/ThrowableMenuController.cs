﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRTK;
using VRTK.UnityEventHelper;

public class ThrowableMenuController : MonoBehaviour {
    public GameObject head;
    public string MenuTitle = "Menü";
    public GameObject preview;
    private List<GameObject> itemList;
    public float objectMenuDistance = 0.5f;
    public GameObject objectTitleReference;
    public GameObject MenuReference;

    public GameObject TennisBallPrefab;
    public GameObject SoccerBallPrefab;
    
    public GameObject CanvasUI;
    public bool IsActuiveMenu = false;

    public GameObject grabbedObject;

    public int ItemSelected = -1;



void Awake()
    {
        preview = GameObject.Find("preview");
        setMenuItemTitle(MenuTitle);
        closeMenu();

        if (GetComponent<VRTK_ControllerEvents>() == null)
        {
            VRTK_Logger.Error(VRTK_Logger.GetCommonMessage(VRTK_Logger.CommonMessageKeys.REQUIRED_COMPONENT_MISSING_FROM_GAMEOBJECT, "VRTK_ControllerEvents_ListenerExample", "VRTK_ControllerEvents", "the same"));
            return;
        }

        GetComponent<VRTK_ControllerEvents>().ButtonTwoPressed += new ControllerInteractionEventHandler(MenuButtonPressed);
    }

    private void MenuButtonPressed(object sender, ControllerInteractionEventArgs e)
    {
        if(!IsActuiveMenu)
        {
            setDefaultItem();
            openMenu();
        } else
        {
            closeMenu();
        }
    }

    // Update is called once per frame
    void FixedUpdate () {
        //smooth spin item
        preview.transform.Rotate(Vector3.up, Time.deltaTime * 20, Space.World);

        
    }

    public void showItem(GameObject prefab, string title, int itemNumber)
    {
        foreach (Transform child in preview.transform)
        {
            Destroy(child.gameObject);
        }


        GameObject item = Instantiate(prefab, preview.transform);
        item.GetComponent<VRTK_InteractableObject>().isGrabbable = false;
        item.GetComponent<Rigidbody>().isKinematic = true;
        preview.transform.parent = item.transform;
        item.transform.position = preview.transform.position;
        setMenuItemTitle(title);
        ItemSelected = itemNumber;

    }



    public void setMenuItemTitle(string title)
    {
        Text t = objectTitleReference.GetComponent<Text>();
        t.text = title;
    }

    public void openMenu()
    {
        CanvasUI.SetActive(true);
        preview.SetActive(true);
        IsActuiveMenu = true;

        //set preview item to right controller
        GameObject previewCopy = Instantiate(getPrefab(ItemSelected), grabbedObject.transform);
        previewCopy.GetComponent<Rigidbody>().isKinematic = true;
        previewCopy.transform.position = grabbedObject.transform.position;
        grabbedObject.transform.parent = previewCopy.transform;
    }

    public void closeMenu()
    {
        CanvasUI.SetActive(false);
        preview.SetActive(false);
        IsActuiveMenu = false;
        
        foreach( Transform child in grabbedObject.transform)
        {
            try
            {
                Destroy(child.gameObject);
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
            }

        }
    }

    public void setDefaultItem()
    {
        showItem(getPrefab(0), "Tennisball", 0);
    }

    public GameObject getPrefab(int id)
    {
        GameObject go = null;

        switch(id)
        {
            case 0:
                go = TennisBallPrefab;
                break;
            case 1:
                go = SoccerBallPrefab;
                break;


            default:
                throw new System.ArgumentNullException("FEHLER");
                break;
        }

        return go;
    }

    private GameObject getPreviewObject()
    {
        GameObject go = null;
        foreach (GameObject child in preview.transform)
        {   
            if (child != null)
            {
                go = child;
            }
        }
        return go;
    }


    public void NextItemClick()
    {
        //
    }

    public void BeforeItemClick()
    {
        //
    }

}
