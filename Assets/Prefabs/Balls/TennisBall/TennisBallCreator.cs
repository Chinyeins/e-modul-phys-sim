﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TennisBallCreator : MonoBehaviour {
    public GameObject TennisBallPrefab;
    public GameObject TennisBall;
    public string title = "Tennisball";

    public GameObject make(Transform parent)
    {
        return Instantiate(TennisBallPrefab, parent);
    }
}
